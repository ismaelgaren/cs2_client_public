const token = localStorage.getItem('token');

document.querySelector('#editCourse').addEventListener('submit', (e) => {
  e.preventDefault();

  let courseId = document.getElementById('courseId').value;
  let courseName = document.getElementById('courseName').value;
  let courseDescription = document.getElementById('courseDescription').value;
  let coursePrice = document.getElementById('coursePrice').value

  if((courseId === "") && (courseName === "")){
    alert('Something went wrong');
  }else{
    fetch('https://mighty-sierra-56067.herokuapp.com/courses', {
      method: 'GET',
      headers: {
        'Content-type' : 'application/json',
        'Authorization' : `Bearer ${token}`
      }
    }).then(res => res.json())
    .then(data => {
    fetch(`https://mighty-sierra-56067.herokuapp.com/courses/:courseId`, {
      method: 'PUT',
      headers: {
        'Content-type' : 'application/json',
        'Authorization' : `Bearer ${token}`
      },
      body: JSON.stringify({
        name: courseName,
        description: courseDescription,
        price: coursePrice
      })
    })
    .then(res => {
      return res.json()
    })
    .then(data => {
      if(data === true){
        alert("Course Update");
      }else{
        alert("Something went wrong")
      }
      })
    })
  }

})