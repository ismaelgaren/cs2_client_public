const signUpButton = document.getElementById('signUp');
const signInButton = document.getElementById('signIn');
const container = document.getElementById('container');

signUpButton.addEventListener('click', () => {
	container.classList.add("right-panel-active");
});

signInButton.addEventListener('click', () => {
	container.classList.remove("right-panel-active");
});

// register
let registerForm = document.querySelector("#registerUser")

registerForm.addEventListener("submit", (e) => {
    e.preventDefault()
    
    let firstName = document.querySelector("#firstName").value
    let lastName = document.querySelector("#lastName").value 
    let mobileNo = document.querySelector("#mobileNumber").value
    let email = document.querySelector("#userEmail").value
    let password1 = document.querySelector("#password1").value
    let password2 = document.querySelector("#password2").value

    //lets create a validation to enable the submit button when all fields are populated and both passwords should match otherwise it should not continue.
    if((password1 !== '' && password2 !== '') && (password2 === password1) && (mobileNo.length === 11)) {

        //if all the requirements are met, then now lets check for duplicate emails in the database first. 
        fetch('https://mighty-sierra-56067.herokuapp.com/api/users/email-exists', {
            method: 'POST', 
            headers: {
                'Content-Type': 'application/json'
            }, 
            body: JSON.stringify({
                email: email,
            })
        })
        .then(res => res.json())
        .then(data => {
            //if no email duplicates are found 
            if(data === false){
                fetch('https://mighty-sierra-56067.herokuapp.com/api/users', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email : email,
                        password: password1,
                        mobileNo: mobileNo
                    })
                })
                .then(res => {
                    return res.json()
                })
                .then(data => {
                    console.log(data)
                    //lets give a proper response if registration will become successful
                    if(data === true){
                        Swal.fire(
                              'Great!',
                              'New Account Registered',
                              'success'
                            )
                        // //redirect to login 
                        // window.location.replace("../pages/login.html")
                    } else {
                        Swal.fire({
                          icon: 'error',
                          title: 'Oops...',
                          text: 'Something went wrong!',
                          footer: '<a href>Why do I have this issue?</a>'
                        })

                    }
                })
            }
        })
    } else {
      alert("Something went wrong, check credentials!")  
    }
})


// =================== LOGIN FORM ==========================
let loginForm = document.querySelector("#loginUser")


loginForm.addEventListener("submit", (e) => {
    e.preventDefault()

    let email = document.querySelector("#userMail").value
    let password  = document.querySelector("#password").value
    // console.log(email)
    // console.log(password)

    //lets now create a control structure that will allow us to determine if there is data inserted in the given fields
    if(email === "" || password === "") {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!',
          footer: '<a href>Why do I have this issue?</a>'
        })
    } else {
        fetch('https://mighty-sierra-56067.herokuapp.com/api/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            }, 
            body: JSON.stringify({
               email: email,
               password: password
            })
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
            console.log(data)
            // successful authentication will return a JWT via response accessToken property
            if(data.access){
                //store JWT in the localStorage
                localStorage.setItem('token', data.access); 
                //send a fetch request to decode the JWT and obtain the user ID and role for storing inside the context
                fetch('https://mighty-sierra-56067.herokuapp.com/api/users/details', {
                    headers: {
                        'Authorization' : `Bearer ${data.access}`
                    }
                })
                .then(res => {
                    return res.json()
                })
                .then(data => {
                    console.log(data)
                    //set the global user state to have properties containing authenticated user ID and Role.
                        localStorage.setItem("id", data._id)
                        localStorage.setItem("isAdmin", data.isAdmin)
                        //once that the id and is admin property of the user is succefully saved in the local storage, then the next task is to redirect the user to the profile page. 
                        
                        if(data.isAdmin !== true){
                            Swal.fire(
                              'Great!',
                              'The system has detected an Guest Account',
                              'success'
                            )
                            window.location.replace("./profile.html")
                        }else{
                            Swal.fire(
                              'Great!',
                              'The system has detected an Admin Account',
                              'success'
                            )
                            window.location.replace("./courses.html")
                        }

                })
            } else {
                //the else branch will run if there us an authentication failure
                alert("Something Went Wrong, Check your Credentials")
            }
        })
    }

})