const token = localStorage.getItem('token');

fetch('https://mighty-sierra-56067.herokuapp.com/api/courses', {
  method: 'GET',
  headers: {
    'Content-type' : 'application/json',
    'Authorization' : `Bearer ${token}`
  }
})
.then(res => res.json())
.then(data => {
console.log(data)
for(let i = 0; i <= data.length; i++){
  fetch(`https://mighty-sierra-56067.herokuapp.com/api/courses/${data.enrollees}`, {
    method: 'GET',
    headers: {
      'Content-type' : 'application/json',
      'Authorization' : `Bearer ${token}`
    }
  })
  .then(res => res.json())
  .then(data => {
    
    document.querySelector('#courseContainer').innerHTML +=
    `<thead>
      <tr>
        <th>User ID</th>
        <th>Enrolled On</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>${data[i].userId}</td>
        <td>${data[i].enrolledOn}</td>
        <td></td>
      </tr>
     </tbody>`
     
    })
  }
})

          