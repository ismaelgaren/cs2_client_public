const token = localStorage.getItem('token');

fetch('https://mighty-sierra-56067.herokuapp.com/api/courses', {
	method: 'GET',
	headers: {
		'Content-type' : 'application/json',
		'Authorization' : `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	console.log(data);
	for(let i = 0; i < data.length; i++){
		document.querySelector('#courseContainer').innerHTML +=
				`
					<div class="col-lg-6">
			        <div class="block margin-bottom-sm">
			            <div class="card-deck">
			              <div class="card">
			                <img src="https://www.rasmussen.edu/-/media/images/blogs/school-of-technology/2020/learn-to-code.jpg?la=en&hash=FE00EF759F3DE1C0345EC71CBB5A2C4273D0A411" class="card-img-top" alt="...">
			                <div class="card-body">
			                  <h5 class="card-title">${data[i].name}</h5>
			                  <p class="card-text">${data[i].description}</p>
			                  <p class="card-text small">${data[i]._id}</p>
			                </div>
			                <div class="card-footer">
			                  <button id="editBtn" class="btn btn-secondary">Edit</button>
			                  <button id="deleteBtn" class="btn btn-danger">Delete</button>
			                </div>
			              </div>
			            </div>
			        </div>
			      </div>
				`
		document.getElementById('deleteBtn').addEventListener('click', (e) => {
			fetch(`https://mighty-sierra-56067.herokuapp.com/api/courses/${data[i]._id}` , {
				method: 'DELETE',
				headers: {
					'Content-type' : 'application/json',
					'Authorization' : `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)
				if(data === true){
					alert("Course Delete")
					window.location.replace('./courses.html')
				}else{
					alert("Something went wrong")
				}
			})
		})
	
	}
})


// function deleteRow(btn) {
// 	fetch('http://localhost:4000/courses')
// }
