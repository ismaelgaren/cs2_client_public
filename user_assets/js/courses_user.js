const token = localStorage.getItem('token');

fetch('https://mighty-sierra-56067.herokuapp.com/api/courses', {
	method: 'GET',
	headers: {
		'Content-type' : 'application/json',
		'Authorization' : `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {

for(let i = 0; i <= data.length; i++){
    document.querySelector('#courseContainer'). innerHTML +=
    `<thead>
      <tr>
        <th scope="col">Course Image</th>
        <th scope="col">Course ID</th>
        <th scope="col">Course Name</th>
        <th scope="col">Course Description</th>
        <th scope="col">Price</th>
      </tr>
    </thead>
    <tbody>
      <tr>
      <td class="w-25">
      <img src="https://techstartups.com/wp-content/uploads/2018/06/Best-Programming-Languages-to-Learn-150x150.jpg">
        <td>${data[i]._id}</td>
        <td>${data[i].name}</td>
        <td>${data[i].description}</td>
        <td>${data[i].price}</td>
      </tr>
     </tbody>`
  }
  
})

document.querySelector('#enrollCourse').addEventListener('submit', (e) => {

  e.preventDefault();

  let courseId = document.getElementById('courseId').value;
  if(courseId !== null){
  fetch('https://mighty-sierra-56067.herokuapp.com/api/users/enroll', {
      method: 'POST',
      headers: {
        'Content-type' : 'application/json',
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify({
        courseId: courseId
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      if(data === true){
        Swal.fire(
          'Great!',
          'You Enrolled A Course!',
          'success'
        )
      }else{
        alert("Something went Wrong")
      }
    })
  }else{
    alert("Hays")
  }
})