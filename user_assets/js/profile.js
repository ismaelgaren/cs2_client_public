const token = localStorage.getItem('token');

fetch('https://mighty-sierra-56067.herokuapp.com/api/users/details', {
    method: 'GET',
    headers: {
        'Content-type' : 'application/json',
        'Authorization' : `Bearer ${token}`
    }
})
.then(res => res.json())
.then(data => {
    console.log(data)
      document.getElementById('profileContainer').innerHTML =
        `
          <div class="avatar"><img src="https://via.placeholder.com/150" class="img-fluid">
            <div class="order dashbg-2">You</div>
          </div><a href="profile.html" class="user-title">
            <h3 class="h5">${data.firstName}</h3>
            <span>${data.lastName}</span></a>
            <p>${data.email}</p>
            <p>Mobile No: ${data.mobileNo}</p>
            <p>Student ID: ${data._id}</p>`

            for(let i = 0; i <= data.enrollments.length; i++){
              fetch(`https://mighty-sierra-56067.herokuapp.com/api/courses/${data.enrollments[i].courseId}` , {
                method: 'GET',
                headers: ({
                  'Content-type' : 'application/json'
                })
              })
              .then(res => res.json())
              .then(data => {
                console.log(data)
                console.log(data.name)
              })
              
              document.querySelector('#courseContainer').innerHTML += 
              `
                <tr>
                  <td class="w-25">
                  <img src="https://techstartups.com/wp-content/uploads/2018/06/Best-Programming-Languages-to-Learn-150x150.jpg">
                  <td>${data.enrollments[i]._id}</td>
                  <td>${data.enrollments[i].enrolledOn}</td>
                  <td>${data.enrollments[i].status}</td>
                </tr>
            `
            }                 
    })

   