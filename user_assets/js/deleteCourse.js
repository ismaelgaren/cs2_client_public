const token = localStorage.getItem('token');

fetch('https://mighty-sierra-56067.herokuapp.com/api/courses', {
	method: 'GET',
	headers: {
		'Content-type' : 'application/json',
		'Authorization' : `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {

for(let i = 0; i <= data.length; i++){
    document.querySelector('#courseContainer').innerHTML +=
    `<tbody>
      <tr>
      <td class="w-25">
      <img src="https://techstartups.com/wp-content/uploads/2018/06/Best-Programming-Languages-to-Learn-150x150.jpg">
        <td>${data[i]._id}</td>
        <td>${data[i].name}</td>
        <td>${data[i].description}</td>
        <td>${data[i].price}</td>
        <td><input id="deleteBtn" class="btn btn-light" type="button" value="Delete" onclick="deleteRow(this)"/></td>
      </tr>
     </tbody>`
  }

})

function deleteRow(btn) {
var row = btn.parentNode.parentNode;
  row.parentNode.removeChild(row);
  Swal.fire(
  'Okay...  ',
  'You Deleted A Course!',
  'success'
   )
}