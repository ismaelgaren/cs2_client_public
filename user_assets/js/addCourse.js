const token = localStorage.getItem('token');
// console.log(token)
const createCourse = document.querySelector('#createCourse');

createCourse.addEventListener('submit', (e) => {
	e.preventDefault();

	let courseName = document.querySelector('#courseName').value;
	let courseDescription = document.querySelector('#courseDescription').value;
	let coursePrice = document.querySelector('#coursePrice').value;

	if((courseName !== "") && (courseDescription !== "") && (coursePrice !== "")){
		fetch('https://mighty-sierra-56067.herokuapp.com/api/courses', {
			method: 'POST',
			headers: {
				'Content-type' : 'application/json',
				'Authorization' : `Bearer ${token}`
			},
			body: JSON.stringify({
				name: courseName,
				description: courseDescription,
				price: coursePrice
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true){
				Swal.fire(
				  'Great!',
				  'You Added A Course!',
				  'success'
				)
				window.location.replace('courses.html')
			}else{
				alert('Course Not Added')
			}
		})
	}else{
		alert("Wrong")
	}
})